# php2exe

![](https://img.shields.io/badge/written%20in-PHP-blue)

a tool to turn command-line php scripts into exe files

Tags: PL

## Overview

php2exe takes your php source code and creates an `.exe` file that has the same effect as running `php file.php`. It is intended to make command-line php scripts easier to distribute.

As an example, `php2exe` is also distributed as an exe file.


```
Usage:
  php2exe [options] file.php

Options:
  --force                          Delete target directory if it exists
  --help                           Display usage
  --no-cleanup                     Don't remove temporary files
  --output {directory}             Set target directory (default 'output')
  --vc-ver {number}                Set target VS version (default '12' ie 2013)
  --version {string}               Set target PHP runtime
                                    (default '5.6.4-nts-Win32-VC11-x86')
```


`php2exe` works by creating a new program that includes PHP as an embedded language interpreter using the `embed` SAPI. Your source code is compiled into the binary and then passed to the embedded interpreter. The source code is obfuscated in the binary, it would take nontrivial effort to extract but it is still recoverable.

`php2exe` does not process multiple files, if you want to process a multi-file project then you should first pack your project into a single file using `phppack`.

Certain library files must be present to build the final `.exe` file. Download the `php-` and `php-devel-pack` zip files for your target runtime version from http://windows.php.net/downloads/releases/archives/ and place them in the same directory as `php2exe`. You can use the `--version` option to specify which zip files should be used.

Visual Studio installation is required to use `php2exe`. It's not required to run the resulting exe, but the Visual C++ runtimes *are* required. It's best if it matches the version of the package so that you are not required to ship two versions of the Visual C++ runtimes. You can let `php2exe` know your Visual Studio version by the `--vc-ver` option (tested with Visual Studio 2013).

By default `php2exe` does not load any modules. You can load modules yourself by the `dl()` function (note that this requires a non-thread-safe build of PHP).


## Download

- [⬇️ php2exe_r08.src.zip](dist-archive/php2exe_r08.src.zip) *(4.21 KiB)*
- [⬇️ php2exe_r08.bin.zip](dist-archive/php2exe_r08.bin.zip) *(2.88 MiB)*
